import { nanoid } from 'nanoid';

// HEAD DATA
export const headData = {
  title: 'Mottaki Hajouni', // e.g: 'Name | Developer'
  lang: 'en,ar,fr', // e.g: en, es, fr, jp
  description: 'Welcome to my website', // e.g: Welcome to my website
};

// HERO DATA
export const heroData = {
  title: '',
  name: 'Mottaki Hajouni',
  subtitle: 'Mobile Developer , Web Developer , DesktopApp Developer ',
  cta: '',
};

// ABOUT DATA
export const aboutData = {
  img: 'nk22 icon.png',
  paragraphOne: 'Mottaki Hajouni from tunisia aged 20y.o aka nizarkratos in gaming models i mean in gaming carrer.',
  paragraphTwo: 'in this 20years i learn alot of stuff and always learn new stuffs and i become a developer with a less experience but keep it in mind always learn and learn to become pro  one ',
  paragraphThree: 'Welcome to my offical website once again i hope you guys like my projects and dont forget to share them  and take a look about it.',
  resume: 'https://drive.google.com/file/d/1WjMhPD7-e8Yk7iKQxc4QzhwcgVUjGxFl/view?usp=sharing', // if no resume, the button will not show up
};

// PROJECTS DATA
export const projectsData = [
  {
    id: nanoid(),
    img: 'fac.png',
    title: 'Facna',
    info: 'Facna is tunisian application founded and created by mottaki hajouni aged 20 y.o studying in iset zaghouan . the main goal of this application is facilite the new students in all university in tunisia founding their own way after succed in baccalauriat also found their best conditions with their own score in the correct university also the application had many features like add events in each university it can help the gouvermenent aswell , got official exams of each year , extra bonus of education like university activity clubs and formations and it will always stay tuned in each period with alot of updates.',
    info2: '',
    url: 'http://facna.ga/',
    repo: 'https://github.com/NizarKratos22/FacnaWebSite', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'HerpeX white.jpg',
    title: 'Herpex camp.',
    info: 'Herpex is tunisian music broadcast making beats , making popular music remix , make instrumental for rap music also  can mastering in last steps of production also herpex is based as an idea mainly in youtube with some video effects and cool desgin depends the music that we are making . ',
    info2: '',
    url: 'https://herpex.netlify.app/',
    repo: 'https://github.com/NizarKratos22/Herpex', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'LOGO.png',
    title: 'CALCULATOR',
    info: 'a basic calculator made by kotlin as programmation language used in android studio .',
    info2: '',
    url: '',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'mhfashion.jpg',
    title: 'MH fasion TN',
    info: 'Mh fashion is a new tunisian clothes brand selling tunisian products like (short, t-shorts , watches ,...) and now mh fashion is a brand of clothes with official  signature it means offical brand of clothes with self production and its growing up step by step . ',
    info2: '',
    url: 'https://www.facebook.com/MH-Fashion-105254418372334',
    repo: '', // if no repo, the button will not show up
  },
  {
    id: nanoid(),
    img: 'nk22 icon.png',
    title: 'NK22 Music',
    info: 'NK22 Music is a youtube channel base in AMV (anime music video) and GMV (game music video) with better codition of motivation music and cool  montage with great  effects . ',
    info2: '',
    url: 'https://www.youtube.com/channel/UCsliDgCvhk0eT1hqNCHbE_g',
    repo: '', // if no repo, the button will not show up
  },
  {

   id: nanoid(),
    img: 'hamemavoyage.png',
    title: 'Hamema Voyage',
    info: 'Hamema voyage is flight services based in tunisia , turquie and algeria giving best price and better flight quality. ',
    info2: '',
    url: 'http://hamemavoyage.epizy.com/',
    repo: 'https://github.com/NizarKratos22/HamemaVoyage.git', // if no repo, the button will not show up
  },
];

// CONTACT DATA
export const contactData = {
  cta: '',
  btn: '',
  email: 'nizar.hajjouni7@gmail.com',
};

// FOOTER DATA
export const footerData = {
  networks: [
    {
      id: nanoid(),
      name: 'twitter',
      url: 'https://twitter.com/krat0_s',
    },
    {
      id: nanoid(),
      name: 'codepen',
      url: 'https://codepen.io/nizarkratos22',
    },
    {
      id: nanoid(),
      name: 'linkedin',
      url: 'https://www.linkedin.com/in/mottaki-hajouni-6361721b3/',
    },
    {
      id: nanoid(),
      name: 'gitlab',
      url: 'https://gitlab.com/NizarKratos22',
    },
    {
      id: nanoid(),
      name: 'github',
      url: 'https://github.com/NizarKratos22',
    },
  ],
};

// Github start/fork buttons
export const githubButtons = {
  isEnabled: false, // set to false to disable the GitHub stars/fork buttons
};
